import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sunfish_ai_game/components/component_collection.dart';

class PageCollection extends StatefulWidget {
  const PageCollection({Key? key}) : super(key: key);

  @override
  State<PageCollection> createState() => _PageCollectionState();

}

class _PageCollectionState extends State<PageCollection> {
  bool _isEnding1 = false;
  bool _isEnding2 = false;
  bool _isEnding3 = false;
  bool _isEnding4 = false;

  void _initEndings() async {
    final prefs = await SharedPreferences.getInstance();
    bool? e1 = prefs.getBool('ending1');
    bool? e2 = prefs.getBool('ending2');
    bool? e3 = prefs.getBool('ending3');
    bool? e4 = prefs.getBool('ending4');

    setState(() {
      _isEnding1 = e1!;
      _isEnding2 = e2!;
      _isEnding3 = e3!;
      _isEnding4 = e4!;
    });
  }

  @override
  void initState() {
    super.initState();
    _initEndings();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          ComponentCollection(imageAddress: 'assets/ending1.png', endingTitle: '즉사', endingContent: '개복치가 과도한 스트레스로 사망했습니다...!', isWatchEnding: _isEnding1, endingNumber: 1,),
          SizedBox(height: 15,),
          ComponentCollection(imageAddress: 'assets/ending2.png', endingTitle: '사망', endingContent: '개복치가 시련을 견디지 못하고 영원한 잠에 빠졌습니다...', isWatchEnding: _isEnding2, endingNumber: 2,),
          SizedBox(height: 15,),
          ComponentCollection(imageAddress: 'assets/ending3.png', endingTitle: '개복치 왕', endingContent: '개복치가 개복치 왕이 되어 떠났습니다.', isWatchEnding: _isEnding3, endingNumber: 3,),
          SizedBox(height: 15,),
          ComponentCollection(imageAddress: 'assets/ending4.png', endingTitle: '평범한 개복치', endingContent: '개복치가 30일간 버텨내긴 했지만, 개복치 왕이 되진 못했습니다.', isWatchEnding: _isEnding4, endingNumber: 4,),
        ],
      )
    );
  }
}
