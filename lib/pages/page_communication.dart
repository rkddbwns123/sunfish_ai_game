import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sunfish_ai_game/components/common/component_custom_loading.dart';
import 'package:sunfish_ai_game/components/common/component_notification.dart';
import 'package:sunfish_ai_game/config/config_form_validator.dart';
import 'package:sunfish_ai_game/repository/repo_game.dart';

class PageCommunication extends StatefulWidget {
  const PageCommunication({Key? key}) : super(key: key);

  @override
  State<PageCommunication> createState() => _PageCommunicationState();
}

class _PageCommunicationState extends State<PageCommunication> {
  final _formKey = GlobalKey<FormBuilderState>();

  String? _aiMessage;
  String? _humanMessage;
  int _lifeCount = 100;
  int _remainDay = 10;
  bool _isDead = false;
  bool _isGameEnd = false;

  void _resetGame() {
    setState(() {
      _aiMessage = null;
      _humanMessage = null;
      _lifeCount = 100;
      _remainDay = 10;
      _isDead = false;
      _isGameEnd = false;
    });
  }

  void _checkEnding() async{
    final prefs = await SharedPreferences.getInstance();
    int endingNumber = 0;

    if(_isDead) {
      prefs.setBool('ending1', true);
      endingNumber = 1;
    } else {
      if(_lifeCount > 0 && _lifeCount <= 30) {
        prefs.setBool('ending2', true);
        endingNumber = 2;
      } else if(_lifeCount > 30 && _lifeCount <= 100) {
        prefs.setBool('ending4', true);
        endingNumber = 4;
      } else {
        prefs.setBool('ending3', true);
        endingNumber = 3;
      }
    }
    _dialogEnding(endingNumber);
  }

  void _initEndings() async {
    final prefs = await SharedPreferences.getInstance();

    bool? e1 = prefs.getBool('ending1');
    bool? e2 = prefs.getBool('ending2');
    bool? e3 = prefs.getBool('ending3');
    bool? e4 = prefs.getBool('ending4');

    if(e1 == null) prefs.setBool('ending1', false);
    if(e2 == null) prefs.setBool('ending2', false);
    if(e3 == null) prefs.setBool('ending3', false);
    if(e4 == null) prefs.setBool('ending4', false);

  }

  Future<void> _sendMessage(String message) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoGame().sendMessage(message).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _aiMessage = res.data.aiFulfillmentText;
        _humanMessage = res.data.humanQueryText;
        _lifeCount += res.data.changeLifeCount;
        _remainDay -= 1;
        _isDead = res.data.isDead;
      });

    }).catchError((err) {
      ComponentNotification(
        success: false,
        title: '전송 실패',
        subTitle: '전송에 실패하였습니다.',
      ).call();

      BotToast.closeAllLoading();
    });
  }

  @override
  void initState() {
    super.initState();
    _initEndings();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _gameView()
    );
  }
  Widget _gameView() {
    if(_isDead || _remainDay <= 0 || _lifeCount <= 0) {
      if(_isGameEnd) {
        return Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.max,
              children: [
                Text('생명력'),
                Container(
                  width: 100,
                  height: 20,
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.black,
                          width: 2
                      )
                  ),
                  child: Text(_lifeCount.toString(), textAlign: TextAlign.center,),
                ),
                Text('남은 일 수'),
                Container(
                  width: 100,
                  height: 20,
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.black,
                          width: 2
                      )
                  ),
                  child: Text(_remainDay.toString(), textAlign: TextAlign.center,),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            SizedBox(
              width: 150,
              height: 150,
              child: Image.asset('assets/base_sunfish.png', fit: BoxFit.fill),
            ),
            const SizedBox(
              height: 30,
            ),
            SingleChildScrollView(
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 200,
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.black,
                          width: 2
                      )
                  ),
                  child: Column(
                    children: [
                      Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            _humanMessage != null ?
                            Text(_humanMessage!, style: TextStyle(color: Colors.black, fontSize: 15, fontWeight: FontWeight.bold), textAlign: TextAlign.right,) : Container(),
                            _humanMessage != null ?
                            Image.asset('assets/user.png', width: 50, height: 50, fit: BoxFit.fill,) : Container(),
                          ]
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          _aiMessage != null ?
                          Image.asset('assets/base_sunfish.png', width: 50, height: 50, fit: BoxFit.fill,) : Container(),
                          _aiMessage != null ?
                          Text(_aiMessage!, style: TextStyle(color: Colors.blue, fontSize: 15, fontWeight: FontWeight.bold), textAlign: TextAlign.left,) : Container(),
                        ],
                      )
                    ],
                  )
              ),
            ),
            OutlinedButton(
              onPressed: () {
                _resetGame();
              },
              child: Text('다시하기'),
            ),
          ],
        );
      }
      return Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.max,
            children: [
              Text('생명력'),
              Container(
                width: 100,
                height: 20,
                decoration: BoxDecoration(
                    border: Border.all(
                        color: Colors.black,
                        width: 2
                    )
                ),
                child: Text(_lifeCount.toString(), textAlign: TextAlign.center,),
              ),
              Text('남은 일 수'),
              Container(
                width: 100,
                height: 20,
                decoration: BoxDecoration(
                    border: Border.all(
                        color: Colors.black,
                        width: 2
                    )
                ),
                child: Text(_remainDay.toString(), textAlign: TextAlign.center,),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          SizedBox(
            width: 150,
            height: 150,
            child: Image.asset('assets/base_sunfish.png', fit: BoxFit.fill),
          ),
          const SizedBox(
            height: 30,
          ),
          SingleChildScrollView(
            child: Container(
                width: MediaQuery.of(context).size.width,
                height: 200,
                decoration: BoxDecoration(
                    border: Border.all(
                        color: Colors.black,
                        width: 2
                    )
                ),
                child: Column(
                  children: [
                    Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          _humanMessage != null ?
                          Text(_humanMessage!, style: TextStyle(color: Colors.black, fontSize: 15, fontWeight: FontWeight.bold), textAlign: TextAlign.right,) : Container(),
                          _humanMessage != null ?
                          Image.asset('assets/user.png', width: 50, height: 50, fit: BoxFit.fill,) : Container(),
                        ]
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        _aiMessage != null ?
                        Image.asset('assets/base_sunfish.png', width: 50, height: 50, fit: BoxFit.fill,) : Container(),
                        _aiMessage != null ?
                        Text(_aiMessage!, style: TextStyle(color: Colors.blue, fontSize: 15, fontWeight: FontWeight.bold), textAlign: TextAlign.left,) : Container(),
                      ],
                    )
                  ],
                )
            ),
          ),
          OutlinedButton(
            onPressed: () {
              _checkEnding();
            },
            child: Text('엔딩보기'),
          ),
        ],
      );
    } else {
      return SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Text('생명력'),
                  Container(
                    width: 100,
                    height: 20,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.black,
                            width: 2
                        )
                    ),
                    child: Text(_lifeCount.toString(), textAlign: TextAlign.center,),
                  ),
                  Text('남은 일 수'),
                  Container(
                    width: 100,
                    height: 20,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.black,
                            width: 2
                        )
                    ),
                    child: Text(_remainDay.toString(), textAlign: TextAlign.center,),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              SizedBox(
                width: 150,
                height: 150,
                child: Image.asset('assets/base_sunfish.png', fit: BoxFit.fill),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  FormBuilder(
                    key: _formKey,
                    autovalidateMode: AutovalidateMode.disabled,
                    child: Flexible(
                      child: FormBuilderTextField(
                        name: 'sendMessage',
                        decoration: InputDecoration(
                          hintText: '대화를 입력하세요!',
                          labelStyle: TextStyle(color: Colors.redAccent),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(width: 1, color: Colors.redAccent),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(width: 1, color: Colors.redAccent),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          ),
                        ),
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.required(errorText: formErrorRequired),
                          FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                          FormBuilderValidators.maxLength(30, errorText: formErrorMaxLength(30))
                        ]),
                        keyboardType: TextInputType.text,
                      ),
                    ),
                  ),
                  OutlinedButton(
                      onPressed: () {
                        if(_formKey.currentState?.saveAndValidate() ?? false) {
                          String resultMessage = _formKey.currentState!.fields['sendMessage']!.value;
                          _sendMessage(resultMessage);
                        }
                      },
                      style: OutlinedButton.styleFrom(padding: EdgeInsets.all(23)),
                      child: Icon(Icons.play_arrow)
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              SingleChildScrollView(
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 200,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.black,
                            width: 2
                        )
                    ),
                    child: Column(
                      children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              _humanMessage != null ?
                              Text(_humanMessage!, style: TextStyle(color: Colors.black, fontSize: 15, fontWeight: FontWeight.bold), textAlign: TextAlign.right,) : Container(),
                              _humanMessage != null ?
                              Image.asset('assets/user.png', width: 50, height: 50, fit: BoxFit.fill,) : Container(),
                            ]
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            _aiMessage != null ?
                            Image.asset('assets/base_sunfish.png', width: 50, height: 50, fit: BoxFit.fill,) : Container(),
                            _aiMessage != null ?
                            Text(_aiMessage!, style: TextStyle(color: Colors.blue, fontSize: 15, fontWeight: FontWeight.bold), textAlign: TextAlign.left,) : Container(),
                          ],
                        )
                      ],
                    )
                ),
              ),
            ],
          ),
        ),
      );
    }
  }
  Future<void> _dialogEnding(int endingNumber) async {
    String endingText = _getEndingText(endingNumber);
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('엔딩 ${endingNumber}'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: 100,
                  height: 100,
                  child: Image.asset(_getImageAddress(endingNumber)),
                ),
                if(endingNumber <= 2) Text('육성 실패') else Text('육성 성공'),
                Text(endingText)
              ],
            ),
            actions: [
              TextButton(
                child: Text('확인'),
                onPressed: () {
                  setState(() {
                    _isGameEnd = true;
                  });
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        }
    );
  }
}
String _getImageAddress(int endingNumber) {
  String result = '';
  if(endingNumber == 1) {
    result = 'assets/ending1.png';
  } else if(endingNumber == 2) {
    result = 'assets/ending2.png';
  } else if(endingNumber == 3) {
    result = 'assets/ending3.png';
  } else {
    result = 'assets/ending4.png';
  }
  return result;
}

String _getEndingText(int endingNumber) {
  String resultContent = '';
  if(endingNumber == 1) {
    resultContent = '개복치가 과도한 스트레스로 인해 사망하였습니다.';
  } else if(endingNumber == 2) {
    resultContent = '개복치가 시련을 견디지 못하고 끝 없는 잠에 빠졌습니다.';
  } else if(endingNumber == 3) {
    resultContent = '개복치가 개복치 왕이 되어 떠났습니다!';
  } else {
    resultContent = '개복치가 30일 간 버텨내긴 했지만, 개복치 왕이 되진 못했습니다.';
  }

  return resultContent;
}
