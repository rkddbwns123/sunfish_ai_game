import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sunfish_ai_game/function/token_lib.dart';

class PageOption extends StatelessWidget {
  const PageOption({Key? key}) : super(key: key);

  void _resetData() async{
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
    prefs.setBool('ending1', false);
    prefs.setBool('ending2', false);
    prefs.setBool('ending3', false);
    prefs.setBool('ending4', false);
  }

  Future<void> _logout(BuildContext context) async {
    TokenLib.logout(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            OutlinedButton(
                onPressed: () {
                  _asyncLogoutConfirmDialog(context);
                },
                child: Text('로그아웃')
            ),
            SizedBox(
              height: 20,
            ),
            OutlinedButton(
                onPressed: () {
                  _asyncConfirmDialog(context);
                },
                child: Text('게임 초기화')
            ),
          ],
        ),
      ),
    );
  }
  Future<void> _asyncConfirmDialog(BuildContext context) async {
    return showDialog<void>(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('게임 초기화'),
            content: Text('초기화 하시겠습니까?'),
            actions: <Widget>[
              TextButton(child: Text('취소'), onPressed: () {
                Navigator.of(context).pop();
              },
              ),
              TextButton(child: Text('확인'), onPressed: () {
                _resetData();
                Navigator.of(context).pop();
              })
            ],
          );
        }
    );
  }
  Future<void> _asyncLogoutConfirmDialog(BuildContext context) async {
    return showDialog<void>(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('로그아웃'),
            content: Text('로그아웃 하시겠습니까?'),
            actions: <Widget>[
              TextButton(
                child: Text('취소'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: Text('확인'),
                onPressed: () {
                  _logout(context);
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        }
    );
  }
}
