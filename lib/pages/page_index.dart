import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:sunfish_ai_game/pages/page_collection.dart';
import 'package:sunfish_ai_game/pages/page_communication.dart';
import 'package:sunfish_ai_game/pages/page_option.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  int _currentIndex = 0;

  final List<BottomNavyBarItem> _navItems = [
    BottomNavyBarItem(
      icon: const Icon(Icons.comment_rounded),
      title: const Text('대화하기'),
      activeColor: Colors.red,
      textAlign: TextAlign.center,
    ),
    BottomNavyBarItem(
      icon: const Icon(Icons.book_rounded),
      title: const Text('엔딩 컬렉션'),
      activeColor: Colors.purpleAccent,
      textAlign: TextAlign.center,
    ),
    BottomNavyBarItem(
      icon: const Icon(Icons.person),
      title: const Text('옵션'),
      activeColor: Colors.pink,
      textAlign: TextAlign.center,
    ),
  ];

  final List<Widget> _widgetPages = [
    PageCommunication(),
    PageCollection(),
    PageOption(),
  ];

  void _onItemTap(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: _widgetPages.elementAt(_currentIndex),
      ),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentIndex,
        showElevation: true,
        itemCornerRadius: 24,
        curve: Curves.easeIn,
        onItemSelected: _onItemTap,
        items: _navItems,
      ),
    );
  }
}