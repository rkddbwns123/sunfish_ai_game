import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:sunfish_ai_game/pages/page_communication.dart';
import 'package:sunfish_ai_game/pages/page_index.dart';

class PageStart extends StatefulWidget {
  const PageStart({Key? key}) : super(key: key);

  @override
  State<PageStart> createState() => _PageStartState();
}

class _PageStartState extends State<PageStart> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            Stack(
              children: [
                SizedBox(
                  child: ImageFiltered(
                    imageFilter: ImageFilter.blur(sigmaY: 3, sigmaX: 3),
                    child: Image.asset(
                      'assets/room.jpg',
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height - 100,
                      fit: BoxFit.fill,),
                  ),
                ),
                Positioned(
                  top: 30,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 50,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.grey,
                            width: 2
                        )
                    ),
                    child: Text('개복치 키우기', style: TextStyle(
                        color: Colors.black, fontSize: 30, fontWeight: FontWeight.bold), textAlign: TextAlign.center
                    ),
                  ),
                ),
                const Positioned(
                    top: 130,
                    child: Text(
                      '어느 날 내게 의문 모를 택배가 하나 도착했다.\n\n'
                          '발신인은 존재하지 않고, 택배 내용에는 어항이라고만 적혀있는.\n\n'
                          '주문한 적도 없는 택배가 도착하자 나는 택배를 다시 한 번 살펴봤지만,\n\n'
                          '주소지에 적힌 건 영락없이 내 집 주소였다.\n\n'
                          '평소 같았다면 반송을 한다거나 상자 그대로 버렸겠지만,\n\n'
                          '왜인지 나는 그날따라 호기심을 참을 수가 없었다.\n\n'
                          '조심조심 택배를 열어보자 그 안에는 어항 하나와 편지 하나가 동봉되어 있었다.\n\n'
                          '"30일 간 개복치를 키워주세요!"\n\n'
                          '아주 간단 명료하지만 이해가 가지 않는 내용.\n\n'
                          '27인치 모니터 정도 크기의 어항에는 편지 내용에 나온 것처럼 개복치 한 마리가 들어있었다.\n\n'
                          '뭐 어쩌라는 건지 궁금해져 편지를 이리저리 살펴보다가 \n\n'
                          '뒷 면에 자세한 내용이 적혀있다는 걸 알게 되었다.\n\n'
                          '"이 개복치는 특별한 개복치입니다.\n\n'
                          '개복치의 왕이 되기 위해선 인간 세계에서 30일 간 죽지 않고 시련을 버텨내야 합니다.\n\n'
                          '개복치에게는 하루에 한 번 말을 걸어 대답을 받을 수 있습니다.\n\n'
                          '30일 동안 개복치를 잘 돌봐 개복치를 왕으로 만들어주세요"\n\n'
                          '허무 맹랑한 내용에 나는 믿지 않았지만,\n\n'
                          '개복치에게 인사를 하자마자 나는 편지의 내용이 사실이라는 걸 믿을 수 밖에 없었다.\n\n'
                          '"안녕! 나는 개복치야! 잘 부탁해!"\n\n'
                          '갑작스레 찾아온 믿지 못할 상황이 현실로 변하는 순간, 나는 결심했다.\n\n'
                          '이왕 이렇게 된 거, 남은 30일, 이 개복치를 잘 키워보겠다고!',
                      style: TextStyle(
                        color: Colors.white, fontSize: 10, fontWeight: FontWeight.bold
                    ),
                      textAlign: TextAlign.center,
                    ),
                  ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            OutlinedButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const PageIndex()));
                },
                child: Text('게임 시작')
            ),
            SizedBox(
              height: 15,
              width: 60,
              child: Text('Made by kyj', style: TextStyle(fontSize: 10), textAlign: TextAlign.center,),
            )
          ],
        ),
      ),
    );
  }
}
