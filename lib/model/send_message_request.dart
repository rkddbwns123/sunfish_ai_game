class SendMessageRequest {
  String message;

  SendMessageRequest(this.message);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['message'] = this.message;

    return data;
  }
}