class SendMessageDetailItem {
  String humanQueryText;
  String aiFulfillmentText;
  bool isDead;
  int changeLifeCount;

  SendMessageDetailItem(this.humanQueryText, this.aiFulfillmentText, this.isDead, this.changeLifeCount);

  factory SendMessageDetailItem.fromJson(Map<String, dynamic> json) {
    return SendMessageDetailItem(
      json['humanQueryText'],
      json['aiFulfillmentText'],
      json['isDead'] as bool,
      json['changeLifeCount']
    );
  }
}