class EndingCollectionItem {
  String imageAddress;
  String endingTitle;
  String endingContent;
  bool isWatchEnding;

  EndingCollectionItem(this.imageAddress, this.endingTitle, this.endingContent, this.isWatchEnding);
}