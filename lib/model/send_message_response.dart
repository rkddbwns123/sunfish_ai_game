import 'package:sunfish_ai_game/model/send_message_detail_item.dart';

class SendMessageResponse {
  bool isSuccess;
  int code;
  String msg;
  SendMessageDetailItem data;

  SendMessageResponse(this.isSuccess, this.code, this.msg, this.data);

  factory SendMessageResponse.fromJson(Map<String, dynamic> json) {
    return SendMessageResponse(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      SendMessageDetailItem.fromJson(json['data'])
    );
  }
}