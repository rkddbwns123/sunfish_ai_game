import 'package:flutter/material.dart';
import 'package:sunfish_ai_game/middleware/middleware_login_check.dart';

class LoginCheck extends StatefulWidget {
  const LoginCheck({Key? key}) : super(key: key);

  @override
  State<LoginCheck> createState() => _LoginCheckState();
}

class _LoginCheckState extends State<LoginCheck> {
  @override
  void initState() {
    super.initState();
    MiddlewareLoginCheck().check(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
