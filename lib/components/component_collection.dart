import 'dart:ui';

import 'package:flutter/material.dart';

class ComponentCollection extends StatelessWidget {
  const ComponentCollection({super.key, required this.imageAddress, required this.endingTitle, required this.endingContent, required this.isWatchEnding, required this.endingNumber});

  final String imageAddress;
  final String endingTitle;
  final String endingContent;
  final bool isWatchEnding;
  final int endingNumber;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 140,
      decoration: BoxDecoration(
          border: Border.all(
              color: Colors.black
          )
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const SizedBox(
            width: 10,
          ),
          Container(
              width: 80,
              height: 80,
              decoration: BoxDecoration(
                  border: Border.all(
                    style: BorderStyle.solid,
                    width: 2,
                    color: Colors.black38,
                  )
              ),
              child: _getImage()
          ),
          const SizedBox(
            width: 25,
          ),
          Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  width: 180,
                  height: 25,
                  padding: const EdgeInsets.all(1),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(
                          style: BorderStyle.solid,
                          width: 2,
                          color: Colors.black38
                      )
                  ),
                  child: isWatchEnding ? Text('Ending.${endingNumber} : ${endingTitle}', textAlign: TextAlign.center,) : Text('')
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                width: 250,
                height: 100,
                padding: const EdgeInsets.all(9),
                decoration: BoxDecoration(
                    border: Border.all(
                        width: 2,
                        color: Colors.black38
                    )
                ),
                child: isWatchEnding ? Text(endingContent,
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black),
                  textAlign: TextAlign.center,) : Text(''),
              )
            ],
          ),
          const SizedBox(
            width: 30,
          ),
          const SizedBox(
            width: 10,
          )
        ],
      ),
    );
  }
  Widget _getImage() {
    if(isWatchEnding == false) {
      return ImageFiltered(
        imageFilter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
        child: Image.asset(imageAddress, fit: BoxFit.fill
        ),
      );
    } else {
      return Image.asset(imageAddress, fit: BoxFit.fill);
    }
  }
}
