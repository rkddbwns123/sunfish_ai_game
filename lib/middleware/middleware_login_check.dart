import 'package:flutter/material.dart';
import 'package:sunfish_ai_game/function/token_lib.dart';
import 'package:sunfish_ai_game/pages/page_index.dart';
import 'package:sunfish_ai_game/pages/page_login.dart';
import 'package:sunfish_ai_game/pages/page_start.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    int? memberId = await TokenLib.getMemberId();

    if (memberId == null) {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin()), (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageStart()), (route) => false);
    }
  }
}
