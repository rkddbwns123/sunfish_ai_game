import 'package:dio/dio.dart';
import 'package:sunfish_ai_game/config/config_api.dart';
import 'package:sunfish_ai_game/model/send_message_request.dart';
import 'package:sunfish_ai_game/model/send_message_response.dart';

class RepoGame {
  Future<SendMessageResponse> sendMessage(String message) async {
    SendMessageRequest request = SendMessageRequest(message);
    const String baseUrl = '$apiUri/game/send';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return SendMessageResponse.fromJson(response.data);
  }
}