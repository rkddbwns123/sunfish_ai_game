import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sunfish_ai_game/components/common/component_notification.dart';
import 'package:sunfish_ai_game/pages/page_login.dart';

class TokenLib {
  static Future<int?> getMemberId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt('memberId');
  }

  static Future<String?> getMemberName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('memberName');
  }

  static void setMemberId(int memberId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt('memberId', memberId);
  }

  static void setMemberName(String memberName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberName', memberName);
  }

  static void logout(BuildContext context) async {
    BotToast.closeAllLoading();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();

    ComponentNotification(
      success: false,
      title: '로그아웃',
      subTitle: '로그아웃되어 로그인 페이지로 이동합니다.',
    ).call();

    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => const PageLogin()),
            (route) => false
    );
  }
}
